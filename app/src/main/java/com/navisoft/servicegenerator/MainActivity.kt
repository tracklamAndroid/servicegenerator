package com.navisoft.servicegenerator

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var mFirebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        FirebaseApp.initializeApp(this)
        initButtons()
    }

    private fun initButtons() {
        btnMake.setOnClickListener {
            startMakeActivity()
        }
        btnScan.setOnClickListener {
            startScanActivity()
        }
    }

    private fun startMakeActivity() {
        val intent = Intent(this, MakeActivity::class.java)
        startActivity(intent)
    }

    private fun startScanActivity() {
        if (isGrantedCamera()) {
            val intent = Intent(this, ScanActivity::class.java)
            startActivity(intent)
        }
    }

    private fun isGrantedCamera(): Boolean {
        return if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), 1)
            false
        } else {
            true
        }
    }
}
