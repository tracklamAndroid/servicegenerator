package com.navisoft.servicegenerator

import java.text.SimpleDateFormat
import java.util.*

object IntermediateData {

    const val password: String = "zDXY[4>9Lv[{m_B4"
    const val code: String = "freon"
    const val jsonStructureID: String ="id"
    const val jsonStructureModel: String ="model"
    const val jsonStructureNotes: String ="notes"
    const val jsonStructureDate: String ="date"

    fun convertLongToTime(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat("HH:mm dd.MM.yyyy", Locale.getDefault())
        return format.format(date)
    }

}