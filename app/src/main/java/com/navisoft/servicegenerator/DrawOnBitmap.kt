package com.navisoft.servicegenerator

import android.graphics.*

class DrawOnBitmap(private val id: String, private val model: String, private val date: String) {

    private lateinit var bitmap: Bitmap

    fun generateNewBitmap(b: Bitmap): Bitmap {
        this.bitmap = b
        val canvas = Canvas(bitmap)

        val paintSmall = Paint()
        val paintLarge = Paint()
        paintSmall.color = Color.BLACK
        paintSmall.textSize = textSizeSmall
        paintSmall.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)

        paintLarge.color = Color.BLACK
        paintLarge.textSize = textSizeLarge
        paintLarge.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)

        canvas.drawBitmap(bitmap, 0F, 0F, paintSmall)
        canvas.drawText("FreonService.com", 68F, textSizeLarge + 12, paintLarge)
        canvas.drawText("Ремонт, заправка автокондиціонерів", 38F, textSizeLarge * 2, paintSmall)
        canvas.drawText("тел. 067-729-85-65, 067-704-57-65", 60F, textSizeLarge * 3 - 6, paintSmall)

        canvas.drawText(id, 60F, textSizeLarge * 13 + 32 + 20, paintSmall)
        canvas.drawText(model, 60F, textSizeLarge * 14 + 32 + 10, paintSmall)
        canvas.drawText(date, 60F, textSizeLarge * 15 + 32, paintSmall)

        return bitmap
    }

    private var textSizeSmall: Float = 32F
    private var textSizeLarge: Float = 64F

}