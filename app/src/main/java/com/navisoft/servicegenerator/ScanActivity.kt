package com.navisoft.servicegenerator

import android.content.Context
import android.os.Bundle
import android.os.Vibrator
import android.util.Base64
import android.util.SparseArray
import android.view.SurfaceHolder
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import kotlinx.android.synthetic.main.activity_scan.*
import org.json.JSONObject
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class ScanActivity : AppCompatActivity() {

    lateinit var cameraSource: CameraSource
    private lateinit var barCode: BarcodeDetector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan)
        qrRun()
    }

    private fun qrRun() {
        barCode = BarcodeDetector.Builder(this)
            .setBarcodeFormats(Barcode.QR_CODE)
            .build()

        cameraSource = CameraSource.Builder(this, barCode)
            .setRequestedPreviewSize(640, 480)
            .setAutoFocusEnabled(true)
            .build()

        surfaceViewCamera.holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceCreated(holder: SurfaceHolder?) {
                cameraSource.start(holder)
            }

            override fun surfaceChanged(
                holder: SurfaceHolder?,
                format: Int,
                width: Int,
                height: Int
            ) {

            }

            override fun surfaceDestroyed(holder: SurfaceHolder?) {
                cameraSource.stop()
            }
        })

        barCode.setProcessor(object : Detector.Processor<Barcode> {
            override fun release() {
                TODO("Not yet implemented")
            }

            override fun receiveDetections(p0: Detector.Detections<Barcode>?) {
                if (p0 != null) {
                    val qrCode: SparseArray<Barcode> = p0.detectedItems
                    if (qrCode.size() != 0) {
                        cameraPost(qrCode)
                    }
                }
            }

            private fun cameraPost(qrCode: SparseArray<Barcode>) {
                tvCamera.post {
                    val vibrator: Vibrator =
                        applicationContext.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                    val str: String = qrCode.valueAt(0).displayValue
                    if (str.contains(IntermediateData.code)) {
                        try {
                            val cryptedText: String = str.removePrefix(IntermediateData.code)
                            val decryptedText = cryptedText.decrypt(IntermediateData.password)
                            val json: JSONObject = JSONObject(decryptedText)
                            val time: Long = json.getLong(IntermediateData.jsonStructureDate)

                            val strBld: StringBuilder = java.lang.StringBuilder()
                            strBld.append(
                                "${getString(R.string.st_id)}: ${json.get(IntermediateData.jsonStructureID)}\n"
                            )
                            strBld.append(
                                "${getString(R.string.st_model)}: ${json.get(IntermediateData.jsonStructureModel)}\n"
                            )
                            strBld.append(
                                "${getString(R.string.st_note)}: ${json.get(IntermediateData.jsonStructureNotes)}\n"
                            )
                            strBld.append(
                                "${getString(R.string.st_date)}: ${IntermediateData.convertLongToTime(time)}"
                            )
                            tvCamera.text = strBld

                            //IntermediateData.textFromCamera = tvCamera.text.toString()
                        } catch (e: Exception) {
                            tvCamera.text = str
                        }
                    } else {
                        tvCamera.text = str

                    }
                }
            }

        })

    }

    private fun String.decrypt(password: String): String {
        val secretKeySpec = SecretKeySpec(password.toByteArray(), "AES")
        val iv = ByteArray(16)
        val charArray = password.toCharArray()
        for (i in charArray.indices) {
            iv[i] = charArray[i].toByte()
        }
        val ivParameterSpec = IvParameterSpec(iv)
        val cipher = Cipher.getInstance("AES/GCM/NoPadding")
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec)
        val decryptedByteValue = cipher.doFinal(Base64.decode(this, Base64.DEFAULT))
        return String(decryptedByteValue)
    }

}