package com.navisoft.servicegenerator

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Base64
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.activity_make.*
import org.json.JSONObject
import java.io.*
import java.util.*
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec


class MakeActivity : AppCompatActivity() {

    private var qrCode: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_make)
        initButton()
    }

    private fun initButton() {
        btnGeterateQR.setOnClickListener {
            val json = generateJSON()
            val str = encodeMsg(json)
            val bitmap = generateQR(str)
            if (bitmap != null) {
                mergeTextAndQrcode(bitmap)
            }
        }
        btnSendQR.setOnClickListener {
            if (qrCode != null) {
                shareFile(activity = this, file = bitmapToFile(this.qrCode!!))
            }
        }
    }

    private fun shareFile(activity: Activity, file: File) {
        val uri = FileProvider.getUriForFile(activity, "com.navisoft.servicegenerator.fileProvider", file)
        val intent = Intent()
        intent.action = Intent.ACTION_SEND
        intent.putExtra(Intent.EXTRA_STREAM, uri)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        intent.type = "image/png"
        activity.startActivity(Intent.createChooser(intent, null))
    }

    private fun bitmapToFile(bitmap: Bitmap): File {
        val file = File(cacheDir, "${UUID.randomUUID()}.png")
        try {
            val stream: OutputStream = BufferedOutputStream(FileOutputStream(file))
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
            stream.flush()
            stream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file
    }

    private fun mergeTextAndQrcode(bm: Bitmap) {
        val bitmap: Bitmap = drawOnBitmap.generateNewBitmap(bm)
        qrCode = bitmap
        imageView.setImageBitmap(bitmap)
    }

    private lateinit var drawOnBitmap: DrawOnBitmap

    private fun generateJSON(): JSONObject {
        val time: Long = Date().time
        val json = JSONObject()
        json.put(IntermediateData.jsonStructureID, etIdAuto.text.toString())
        json.put(IntermediateData.jsonStructureModel, etModelAvto.text.toString())
        json.put(IntermediateData.jsonStructureNotes, etMemoAvto.text.toString())
        json.put(IntermediateData.jsonStructureDate, time)

        drawOnBitmap = DrawOnBitmap(
            "${getString(R.string.st_id)}: ${etIdAuto.text}",
            "${getString(R.string.st_model)}: ${etModelAvto.text}",
            "${getString(R.string.st_date)}: ${IntermediateData.convertLongToTime(time)}"
        )

        return json
    }

    private fun encodeMsg(json: JSONObject): String {
        val str = json.toString()
        return str.encrypt(IntermediateData.password)
    }

    private fun generateQR(msg: String): Bitmap? {
        //String text="" // Whatever you need to encode in the QR code
        val multiFormatWriter: MultiFormatWriter = MultiFormatWriter()
        try {
            val bitMatrix: BitMatrix =
                multiFormatWriter.encode(
                    IntermediateData.code + msg,
                    BarcodeFormat.QR_CODE,
                    640,
                    1024
                )
            val barcodeEncoder: BarcodeEncoder = BarcodeEncoder()
            val bitmap: Bitmap = barcodeEncoder.createBitmap(bitMatrix)
            hideKeyboard()
            return bitmap
        } catch (e: WriterException) {
            e.printStackTrace()
            return null
        }
    }

    private fun Activity.hideKeyboard() {
        try {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        } catch (e: KotlinNullPointerException) {

        }
    }

    private fun String.encrypt(password: String): String {
        val secretKeySpec = SecretKeySpec(password.toByteArray(), "AES")
        val iv = ByteArray(16)
        val charArray = password.toCharArray()
        for (i in charArray.indices) {
            iv[i] = charArray[i].toByte()
        }
        val ivParameterSpec = IvParameterSpec(iv)
        val cipher = Cipher.getInstance("AES/GCM/NoPadding")
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec)
        val encryptedValue: ByteArray = cipher.doFinal(this.toByteArray())
        return Base64.encodeToString(encryptedValue, Base64.DEFAULT)
    }


}